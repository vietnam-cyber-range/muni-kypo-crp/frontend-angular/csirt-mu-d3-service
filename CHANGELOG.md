### 16.0.0 Update to Angular 16
* b8887f7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   05013c5 -- Merge branch '20-update-to-angular-16' into 'master'
|\  
| * 4d53f3b -- Update to Angular 16
|/  
* d5c9c69 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a691faa -- [CI/CD] Update packages.json version based on GitLab tag.
* 16019fc -- Allow for fixed deploy script
*   b1a5e3d -- Merge branch 'master' of gitlab.ics.muni.cz:muni-kypo-crp/frontend-angular/csirt-mu-d3-service
|\  
| * 9954736 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
| * 1219e92 -- [CI/CD] Update packages.json version based on GitLab tag.
* | d0cc721 -- Restore CI
|/  
* dc6ec8f -- Update esbuild dependency
* 20675f7 -- Update CI deploy script
* 0227eb8 -- Update .gitlab-ci.yml file
* 2b0d729 -- Add esbuild as optional dependency
* c388f59 -- Add fsevents as optional dependency
* cb3e50b -- Increase version
*   0778e8d -- Merge branch '19-update-to-angular-15' into 'master'
|\  
| * 508aa0e -- Update lockfile
| * baf77e1 -- Update pipeline CI image node version
| * d810976 -- Update angular
| * 39e5c9b -- Update typescript dependency
|/  
* 5dbc53e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c4c307e -- [CI/CD] Update packages.json version based on GitLab tag.
*   1b9286a -- Merge branch '18-update-to-angular-14' into 'master'
|\  
| * 4d3426d -- Resolve "Update to Angular 14"
|/  
* ae152a0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 21a1da3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d3c3a4f -- Merge branch '17-update-to-angular-13' into 'master'
|\  
| * 25cf9d3 -- Resolve "Update to Angular 13"
|/  
* 00fece6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70ac0db -- [CI/CD] Update packages.json version based on GitLab tag.
*   330d280 -- Merge branch '16-update-to-d3v7' into 'master'
|\  
| * af51095 -- Resolve "Update to d3v7"
|/  
*   bc9c078 -- Merge branch '15-add-license-file' into 'master'
|\  
| * c0f8767 -- Add license file
|/  
*   e69a00f -- Merge branch '14-update-d3-version' into 'master'
|\  
| * 173717e -- Resolve "Update d3 version"
|/  
* cf4a4a5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 23e4359 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36bf9db -- Merge branch '13-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * ecdf656 -- Update gitlab CI
|/  
* f6835f1 -- Update project package.json version based on GitLab tag. Done by CI
*   89fae4e -- Merge branch '12-update-to-angular-12' into 'master'
|\  
| * 4942df4 -- Update to Angular 12
|/  
* 069cb3b -- Update project package.json version based on GitLab tag. Done by CI
*   067764d -- Merge branch '11-update-peerdependencies-to-angular-11' into 'master'
|\  
| * d2f6da6 -- Resolve "Update peerDependencies to Angular 11"
|/  
* 6bd185b -- Update project package.json version based on GitLab tag. Done by CI
*   cce6990 -- Merge branch '10-update-to-angular-11' into 'master'
|\  
| * 5dcb6a2 -- Update to Angular 11
|/  
*   7d6b7dd -- Merge branch '9-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 1d6f3e3 -- recreate package lock
|/  
*   d16441e -- Merge branch '8-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 48d1291 -- Migrate to eslint
|/  
* 371b131 -- Update project package.json version based on GitLab tag. Done by CI
*   adc5b6d -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 137c180 -- Renamed the scope
|/  
* 9cd4fd5 -- Update project package.json version based on GitLab tag. Done by CI
*   4ab5f27 -- Merge branch '6-rename-package-to-kypo-d3-service' into 'master'
|\  
| * 7c263ae -- Rename package
|/  
*   18ffd42 -- Merge branch '5-use-cypress-docker-image-in-ci' into 'master'
|\  
| * 767e91c -- Resolve "Use cypress docker image in CI"
|/  
* 37525b9 -- Update project package.json version based on GitLab tag. Done by CI
*   50cbcd8 -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 025b34f -- Update to Angular 10
|/  
*   33973f6 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-parameter' into 'master'
|\  
| * cadeba5 -- Update .gitlab-ci.yml
|/  
* ee5b679 -- Merge branch '2-add-ci' into 'master'
### 15.0.1 Update CI for deploy
* a691faa -- [CI/CD] Update packages.json version based on GitLab tag.
* 16019fc -- Allow for fixed deploy script
*   b1a5e3d -- Merge branch 'master' of gitlab.ics.muni.cz:muni-kypo-crp/frontend-angular/csirt-mu-d3-service
|\  
| * 9954736 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
| * 1219e92 -- [CI/CD] Update packages.json version based on GitLab tag.
* | d0cc721 -- Restore CI
|/  
* dc6ec8f -- Update esbuild dependency
* 20675f7 -- Update CI deploy script
* 0227eb8 -- Update .gitlab-ci.yml file
* 2b0d729 -- Add esbuild as optional dependency
* c388f59 -- Add fsevents as optional dependency
* cb3e50b -- Increase version
*   0778e8d -- Merge branch '19-update-to-angular-15' into 'master'
|\  
| * 508aa0e -- Update lockfile
| * baf77e1 -- Update pipeline CI image node version
| * d810976 -- Update angular
| * 39e5c9b -- Update typescript dependency
|/  
* 5dbc53e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c4c307e -- [CI/CD] Update packages.json version based on GitLab tag.
*   1b9286a -- Merge branch '18-update-to-angular-14' into 'master'
|\  
| * 4d3426d -- Resolve "Update to Angular 14"
|/  
* ae152a0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 21a1da3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d3c3a4f -- Merge branch '17-update-to-angular-13' into 'master'
|\  
| * 25cf9d3 -- Resolve "Update to Angular 13"
|/  
* 00fece6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70ac0db -- [CI/CD] Update packages.json version based on GitLab tag.
*   330d280 -- Merge branch '16-update-to-d3v7' into 'master'
|\  
| * af51095 -- Resolve "Update to d3v7"
|/  
*   bc9c078 -- Merge branch '15-add-license-file' into 'master'
|\  
| * c0f8767 -- Add license file
|/  
*   e69a00f -- Merge branch '14-update-d3-version' into 'master'
|\  
| * 173717e -- Resolve "Update d3 version"
|/  
* cf4a4a5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 23e4359 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36bf9db -- Merge branch '13-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * ecdf656 -- Update gitlab CI
|/  
* f6835f1 -- Update project package.json version based on GitLab tag. Done by CI
*   89fae4e -- Merge branch '12-update-to-angular-12' into 'master'
|\  
| * 4942df4 -- Update to Angular 12
|/  
* 069cb3b -- Update project package.json version based on GitLab tag. Done by CI
*   067764d -- Merge branch '11-update-peerdependencies-to-angular-11' into 'master'
|\  
| * d2f6da6 -- Resolve "Update peerDependencies to Angular 11"
|/  
* 6bd185b -- Update project package.json version based on GitLab tag. Done by CI
*   cce6990 -- Merge branch '10-update-to-angular-11' into 'master'
|\  
| * 5dcb6a2 -- Update to Angular 11
|/  
*   7d6b7dd -- Merge branch '9-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 1d6f3e3 -- recreate package lock
|/  
*   d16441e -- Merge branch '8-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 48d1291 -- Migrate to eslint
|/  
* 371b131 -- Update project package.json version based on GitLab tag. Done by CI
*   adc5b6d -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 137c180 -- Renamed the scope
|/  
* 9cd4fd5 -- Update project package.json version based on GitLab tag. Done by CI
*   4ab5f27 -- Merge branch '6-rename-package-to-kypo-d3-service' into 'master'
|\  
| * 7c263ae -- Rename package
|/  
*   18ffd42 -- Merge branch '5-use-cypress-docker-image-in-ci' into 'master'
|\  
| * 767e91c -- Resolve "Use cypress docker image in CI"
|/  
* 37525b9 -- Update project package.json version based on GitLab tag. Done by CI
*   50cbcd8 -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 025b34f -- Update to Angular 10
|/  
*   33973f6 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-parameter' into 'master'
|\  
| * cadeba5 -- Update .gitlab-ci.yml
|/  
* ee5b679 -- Merge branch '2-add-ci' into 'master'
### 15.0.0 Update to Angular 15 and fix optional dependencies
* 1219e92 -- [CI/CD] Update packages.json version based on GitLab tag.
* dc6ec8f -- Update esbuild dependency
* 20675f7 -- Update CI deploy script
* 0227eb8 -- Update .gitlab-ci.yml file
* 2b0d729 -- Add esbuild as optional dependency
* c388f59 -- Add fsevents as optional dependency
* cb3e50b -- Increase version
*   0778e8d -- Merge branch '19-update-to-angular-15' into 'master'
|\  
| * 508aa0e -- Update lockfile
| * baf77e1 -- Update pipeline CI image node version
| * d810976 -- Update angular
| * 39e5c9b -- Update typescript dependency
|/  
* 5dbc53e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c4c307e -- [CI/CD] Update packages.json version based on GitLab tag.
*   1b9286a -- Merge branch '18-update-to-angular-14' into 'master'
|\  
| * 4d3426d -- Resolve "Update to Angular 14"
|/  
* ae152a0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 21a1da3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d3c3a4f -- Merge branch '17-update-to-angular-13' into 'master'
|\  
| * 25cf9d3 -- Resolve "Update to Angular 13"
|/  
* 00fece6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70ac0db -- [CI/CD] Update packages.json version based on GitLab tag.
*   330d280 -- Merge branch '16-update-to-d3v7' into 'master'
|\  
| * af51095 -- Resolve "Update to d3v7"
|/  
*   bc9c078 -- Merge branch '15-add-license-file' into 'master'
|\  
| * c0f8767 -- Add license file
|/  
*   e69a00f -- Merge branch '14-update-d3-version' into 'master'
|\  
| * 173717e -- Resolve "Update d3 version"
|/  
* cf4a4a5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 23e4359 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36bf9db -- Merge branch '13-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * ecdf656 -- Update gitlab CI
|/  
* f6835f1 -- Update project package.json version based on GitLab tag. Done by CI
*   89fae4e -- Merge branch '12-update-to-angular-12' into 'master'
|\  
| * 4942df4 -- Update to Angular 12
|/  
* 069cb3b -- Update project package.json version based on GitLab tag. Done by CI
*   067764d -- Merge branch '11-update-peerdependencies-to-angular-11' into 'master'
|\  
| * d2f6da6 -- Resolve "Update peerDependencies to Angular 11"
|/  
* 6bd185b -- Update project package.json version based on GitLab tag. Done by CI
*   cce6990 -- Merge branch '10-update-to-angular-11' into 'master'
|\  
| * 5dcb6a2 -- Update to Angular 11
|/  
*   7d6b7dd -- Merge branch '9-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 1d6f3e3 -- recreate package lock
|/  
*   d16441e -- Merge branch '8-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 48d1291 -- Migrate to eslint
|/  
* 371b131 -- Update project package.json version based on GitLab tag. Done by CI
*   adc5b6d -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 137c180 -- Renamed the scope
|/  
* 9cd4fd5 -- Update project package.json version based on GitLab tag. Done by CI
*   4ab5f27 -- Merge branch '6-rename-package-to-kypo-d3-service' into 'master'
|\  
| * 7c263ae -- Rename package
|/  
*   18ffd42 -- Merge branch '5-use-cypress-docker-image-in-ci' into 'master'
|\  
| * 767e91c -- Resolve "Use cypress docker image in CI"
|/  
* 37525b9 -- Update project package.json version based on GitLab tag. Done by CI
*   50cbcd8 -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 025b34f -- Update to Angular 10
|/  
*   33973f6 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-parameter' into 'master'
|\  
| * cadeba5 -- Update .gitlab-ci.yml
|/  
* ee5b679 -- Merge branch '2-add-ci' into 'master'
### 14.0.0 Update to Angular 14
* c4c307e -- [CI/CD] Update packages.json version based on GitLab tag.
*   1b9286a -- Merge branch '18-update-to-angular-14' into 'master'
|\  
| * 4d3426d -- Resolve "Update to Angular 14"
|/  
* ae152a0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 21a1da3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d3c3a4f -- Merge branch '17-update-to-angular-13' into 'master'
|\  
| * 25cf9d3 -- Resolve "Update to Angular 13"
|/  
* 00fece6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70ac0db -- [CI/CD] Update packages.json version based on GitLab tag.
*   330d280 -- Merge branch '16-update-to-d3v7' into 'master'
|\  
| * af51095 -- Resolve "Update to d3v7"
|/  
*   bc9c078 -- Merge branch '15-add-license-file' into 'master'
|\  
| * c0f8767 -- Add license file
|/  
*   e69a00f -- Merge branch '14-update-d3-version' into 'master'
|\  
| * 173717e -- Resolve "Update d3 version"
|/  
* cf4a4a5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 23e4359 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36bf9db -- Merge branch '13-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * ecdf656 -- Update gitlab CI
|/  
* f6835f1 -- Update project package.json version based on GitLab tag. Done by CI
*   89fae4e -- Merge branch '12-update-to-angular-12' into 'master'
|\  
| * 4942df4 -- Update to Angular 12
|/  
* 069cb3b -- Update project package.json version based on GitLab tag. Done by CI
*   067764d -- Merge branch '11-update-peerdependencies-to-angular-11' into 'master'
|\  
| * d2f6da6 -- Resolve "Update peerDependencies to Angular 11"
|/  
* 6bd185b -- Update project package.json version based on GitLab tag. Done by CI
*   cce6990 -- Merge branch '10-update-to-angular-11' into 'master'
|\  
| * 5dcb6a2 -- Update to Angular 11
|/  
*   7d6b7dd -- Merge branch '9-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 1d6f3e3 -- recreate package lock
|/  
*   d16441e -- Merge branch '8-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 48d1291 -- Migrate to eslint
|/  
* 371b131 -- Update project package.json version based on GitLab tag. Done by CI
*   adc5b6d -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 137c180 -- Renamed the scope
|/  
* 9cd4fd5 -- Update project package.json version based on GitLab tag. Done by CI
*   4ab5f27 -- Merge branch '6-rename-package-to-kypo-d3-service' into 'master'
|\  
| * 7c263ae -- Rename package
|/  
*   18ffd42 -- Merge branch '5-use-cypress-docker-image-in-ci' into 'master'
|\  
| * 767e91c -- Resolve "Use cypress docker image in CI"
|/  
* 37525b9 -- Update project package.json version based on GitLab tag. Done by CI
*   50cbcd8 -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 025b34f -- Update to Angular 10
|/  
*   33973f6 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-parameter' into 'master'
|\  
| * cadeba5 -- Update .gitlab-ci.yml
|/  
* ee5b679 -- Merge branch '2-add-ci' into 'master'
### 13.0.0 Update to Angular 13 and CI update
* 21a1da3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d3c3a4f -- Merge branch '17-update-to-angular-13' into 'master'
|\  
| * 25cf9d3 -- Resolve "Update to Angular 13"
|/  
* 00fece6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70ac0db -- [CI/CD] Update packages.json version based on GitLab tag.
*   330d280 -- Merge branch '16-update-to-d3v7' into 'master'
|\  
| * af51095 -- Resolve "Update to d3v7"
|/  
*   bc9c078 -- Merge branch '15-add-license-file' into 'master'
|\  
| * c0f8767 -- Add license file
|/  
*   e69a00f -- Merge branch '14-update-d3-version' into 'master'
|\  
| * 173717e -- Resolve "Update d3 version"
|/  
* cf4a4a5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 23e4359 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36bf9db -- Merge branch '13-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * ecdf656 -- Update gitlab CI
|/  
* f6835f1 -- Update project package.json version based on GitLab tag. Done by CI
*   89fae4e -- Merge branch '12-update-to-angular-12' into 'master'
|\  
| * 4942df4 -- Update to Angular 12
|/  
* 069cb3b -- Update project package.json version based on GitLab tag. Done by CI
*   067764d -- Merge branch '11-update-peerdependencies-to-angular-11' into 'master'
|\  
| * d2f6da6 -- Resolve "Update peerDependencies to Angular 11"
|/  
* 6bd185b -- Update project package.json version based on GitLab tag. Done by CI
*   cce6990 -- Merge branch '10-update-to-angular-11' into 'master'
|\  
| * 5dcb6a2 -- Update to Angular 11
|/  
*   7d6b7dd -- Merge branch '9-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 1d6f3e3 -- recreate package lock
|/  
*   d16441e -- Merge branch '8-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 48d1291 -- Migrate to eslint
|/  
* 371b131 -- Update project package.json version based on GitLab tag. Done by CI
*   adc5b6d -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 137c180 -- Renamed the scope
|/  
* 9cd4fd5 -- Update project package.json version based on GitLab tag. Done by CI
*   4ab5f27 -- Merge branch '6-rename-package-to-kypo-d3-service' into 'master'
|\  
| * 7c263ae -- Rename package
|/  
*   18ffd42 -- Merge branch '5-use-cypress-docker-image-in-ci' into 'master'
|\  
| * 767e91c -- Resolve "Use cypress docker image in CI"
|/  
* 37525b9 -- Update project package.json version based on GitLab tag. Done by CI
*   50cbcd8 -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 025b34f -- Update to Angular 10
|/  
*   33973f6 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-parameter' into 'master'
|\  
| * cadeba5 -- Update .gitlab-ci.yml
|/  
* ee5b679 -- Merge branch '2-add-ci' into 'master'
### 12.0.2 Update to d3 v7
* 70ac0db -- [CI/CD] Update packages.json version based on GitLab tag.
*   330d280 -- Merge branch '16-update-to-d3v7' into 'master'
|\  
| * af51095 -- Resolve "Update to d3v7"
|/  
*   bc9c078 -- Merge branch '15-add-license-file' into 'master'
|\  
| * c0f8767 -- Add license file
|/  
*   e69a00f -- Merge branch '14-update-d3-version' into 'master'
|\  
| * 173717e -- Resolve "Update d3 version"
|/  
* cf4a4a5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 23e4359 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36bf9db -- Merge branch '13-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * ecdf656 -- Update gitlab CI
|/  
* f6835f1 -- Update project package.json version based on GitLab tag. Done by CI
*   89fae4e -- Merge branch '12-update-to-angular-12' into 'master'
|\  
| * 4942df4 -- Update to Angular 12
|/  
* 069cb3b -- Update project package.json version based on GitLab tag. Done by CI
*   067764d -- Merge branch '11-update-peerdependencies-to-angular-11' into 'master'
|\  
| * d2f6da6 -- Resolve "Update peerDependencies to Angular 11"
|/  
* 6bd185b -- Update project package.json version based on GitLab tag. Done by CI
*   cce6990 -- Merge branch '10-update-to-angular-11' into 'master'
|\  
| * 5dcb6a2 -- Update to Angular 11
|/  
*   7d6b7dd -- Merge branch '9-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 1d6f3e3 -- recreate package lock
|/  
*   d16441e -- Merge branch '8-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 48d1291 -- Migrate to eslint
|/  
* 371b131 -- Update project package.json version based on GitLab tag. Done by CI
*   adc5b6d -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 137c180 -- Renamed the scope
|/  
* 9cd4fd5 -- Update project package.json version based on GitLab tag. Done by CI
*   4ab5f27 -- Merge branch '6-rename-package-to-kypo-d3-service' into 'master'
|\  
| * 7c263ae -- Rename package
|/  
*   18ffd42 -- Merge branch '5-use-cypress-docker-image-in-ci' into 'master'
|\  
| * 767e91c -- Resolve "Use cypress docker image in CI"
|/  
* 37525b9 -- Update project package.json version based on GitLab tag. Done by CI
*   50cbcd8 -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 025b34f -- Update to Angular 10
|/  
*   33973f6 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-parameter' into 'master'
|\  
| * cadeba5 -- Update .gitlab-ci.yml
|/  
* ee5b679 -- Merge branch '2-add-ci' into 'master'
### 12.0.1 Update gitlab CI
* 23e4359 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36bf9db -- Merge branch '13-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * ecdf656 -- Update gitlab CI
|/  
* f6835f1 -- Update project package.json version based on GitLab tag. Done by CI
*   89fae4e -- Merge branch '12-update-to-angular-12' into 'master'
|\  
| * 4942df4 -- Update to Angular 12
|/  
* 069cb3b -- Update project package.json version based on GitLab tag. Done by CI
*   067764d -- Merge branch '11-update-peerdependencies-to-angular-11' into 'master'
|\  
| * d2f6da6 -- Resolve "Update peerDependencies to Angular 11"
|/  
* 6bd185b -- Update project package.json version based on GitLab tag. Done by CI
*   cce6990 -- Merge branch '10-update-to-angular-11' into 'master'
|\  
| * 5dcb6a2 -- Update to Angular 11
|/  
*   7d6b7dd -- Merge branch '9-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 1d6f3e3 -- recreate package lock
|/  
*   d16441e -- Merge branch '8-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 48d1291 -- Migrate to eslint
|/  
* 371b131 -- Update project package.json version based on GitLab tag. Done by CI
*   adc5b6d -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 137c180 -- Renamed the scope
|/  
* 9cd4fd5 -- Update project package.json version based on GitLab tag. Done by CI
*   4ab5f27 -- Merge branch '6-rename-package-to-kypo-d3-service' into 'master'
|\  
| * 7c263ae -- Rename package
|/  
*   18ffd42 -- Merge branch '5-use-cypress-docker-image-in-ci' into 'master'
|\  
| * 767e91c -- Resolve "Use cypress docker image in CI"
|/  
* 37525b9 -- Update project package.json version based on GitLab tag. Done by CI
*   50cbcd8 -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 025b34f -- Update to Angular 10
|/  
*   33973f6 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-parameter' into 'master'
|\  
| * cadeba5 -- Update .gitlab-ci.yml
|/  
* ee5b679 -- Merge branch '2-add-ci' into 'master'
